#! /bin/bash

# Copyright 2017  Vimal Manohar
# Apache 2.0.

if [ $# -ne 3 ]; then
  echo "Usage: $0 <wav-dir> <transcript-dir> <out-data>"
  echo " e.g.: $0 /export/a15/vmanoha1/MGB/adapt.20170322/wav /export/a15/vmanoha1/MGB/adapt.20170322/Mohamed data/mgb3_adapt_Mohamed"
  exit 1
fi

wavDir=$1
transcriptDir=$2
outData=$3

for f in wav.scp feats.scp cmvn.scp text reco2file_and_channel; do
  if [ -f $outData/$f ]; then
    mkdir -p $outData/.backup
    mv $outData/$f $outData/.backup
  fi
done

mkdir -p $outData

for x in $wavDir/*.wav; do
  y=`basename $x .wav`
  echo "$y $x" >> $outData/wav.scp
done

if [ -f $transcriptDir/text_noverlap.bw ]; then
  perl -pe 's/ <s> / <UNK> /g' $transcriptDir/text_noverlap.bw | perl -pe 's/ <TRUNC> / /g' >> $outData/text
fi

if [ -f $transcriptDir/text_overlap.bw ]; then
  perl -pe 's/ <s> / <UNK> /g' $transcriptDir/text_overlap.bw | perl -pe 's/ <TRUNC> / /g' >> $outData/text
fi

cp $transcriptDir/segments $outData

awk '{print $1" "$2}' $outData/segments > $outData/utt2spk
cp $outData/utt2spk $outData/spk2utt

awk '{print $1" "$1" 1"}' $outData/wav.scp > $outData/reco2file_and_channel

utils/fix_data_dir.sh $outData
