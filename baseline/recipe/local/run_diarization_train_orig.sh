#!/bin/bash

# Copyright 2016  David Snyder
#           2017  Vimal Manohar
# Apache 2.0.
#
# See README for more info on the required data.

. cmd.sh
. path.sh

set -e 

mfccdir=`pwd`/mfcc_spkrid_16k
vaddir=`pwd`/mfcc_spkrid_16k
num_components=2048
ivector_dim=400

suffix=_a_delta2_s0.3_mc0
ivector_suffix=_rand3_20_20
diarization_opts="--use-vad true --delta-order 2 --posterior-scale 0.3 --max-count 0 --min-chunk-duration 3 --max-chunk-duration 20 --intersegment-duration 20"

stage=-1

. utils/parse_options.sh

set -e
set -u
set -o pipefail

#for f in data/train_1200h_mer80/utt2spk \
#  data/train_orig/wav.scp data/train_1200h_mer80/utt2spk; do 
#  [ ! -f $f ] && echo "Could not find file $f" && exit 1
#done

false && {
bash -x steps/diarization/prepare_diarization.sh --dataset train_1200h_mer80 \
  --exp exp/mer80 --stage 8 \
  --suffix $suffix --ivector-suffix $ivector_suffix \
  --mfccdir $mfccdir --vaddir $mfccdir \
  --diagubm-subset-utts 16000 --fullubm-subset-utts 32000 \
  --num-components $num_components --ivector-dim $ivector_dim \
  $diarization_opts \
  --train-plda-opts "--utts-per-spk-min 2"
}

data=data/train_orig.seg_stats_sad_music_snr_1j
steps/diarization/do_diarization_datadir.sh --nj 80 --reco-nj 40 --cmd "queue.pl" \
  --get-uniform-subsegments false --do-change-point-detection true \
  --change-point-split-opts "--use-full-covar --distance-metric=glr" \
  --change-point-merge-opts "--use-full-covar --distance-metric=bic --threshold=2.0" \
  --ivector-opts "--posterior-scale 0.3 --max-count 0" \
  --per-spk true --use-vad true \
  --calibration-method SingleGaussian --calibrate-per-reco true \
  --target-energy 0.9 \
  --use-src-mean false --use-src-transform false --plda-suffix "" \
  --cluster-method plda \
  --distance-threshold "" --stage 5 \
  $data exp/mer80/extractor_a_delta2_s0.3_mc0_train_1200h_mer80_spkrid_c${num_components}_i${ivector_dim} \
  exp/mer80/ivectors_a_delta2_s0.3_mc0_spkrid_i400_train_1200h_mer80_spkrid_rand3_20_20_vad/ \
  exp/diarization_mer80/diarization_a_delta2_s0.3_mc0_train_1200h_mer80_c${num_components}_i${ivector_dim}_$(basename $data){,/train_orig.diarized}
