#! /bin/bash

# Copyright  2017 Vimal Manohar
# Apache 2.0

set -e -o pipefail -u

stage=-17
nj=60
decode_nj=30
chunk_left_context=40
chunk_right_context=0
label_delay=5
xent_regularize=0.1
train_set=train_reseg_1e
nnet3_affix=_mer80_multi_mgb3      # cleanup affix for nnet3 and chain dirs, e.g. _cleaned
# decode options
extra_left_context=50
extra_right_context=0
frames_per_chunk=150

# The rest are configs specific to this script.  Most of the parameters
# are just hardcoded at this level, in the commands below.
train_stage=-10
tree_dir=exp/chain_segmented_1e/tree_bi
src_dir0=exp/chain_segmented_1e/tdnn_lstm_1a_sp_bi
src_dir1=exp/chain_mer80_adapt_mgb3/tdnn_lstm_1b2_sp_bi

lat_dir=exp/chain_segmented_1e/mer80/tri4_train_reseg_1e_sp_comb_lats
train_ivector_dir=exp/nnet3_segmented_1e/ivectors_train_reseg_1e_sp_hires_comb
tdnn_lstm_affix=1a  #affix for TDNN-LSTM directory, e.g. "a" or "b", in case we change the configuration.
common_egs_dir=
extractor=  # use previously trained extractor

# End configuration section.
echo "$0 $@"  # Print the command line for logging

if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

. cmd.sh
. ./path.sh
. ./utils/parse_options.sh

train_data_dir=data/${train_set}_sp_hires_comb
dir=exp/chain${nnet3_affix}/tdnn_lstm_${tdnn_lstm_affix}_sp_bi

if [ -z "$common_egs_dir" ]; then
  if [ $stage -le 16 ]; then
    steps/nnet3/multilingual/combine_egs.sh --cmd "$train_cmd" \
      --minibatch-size 128 --samples-per-iter 10000 \
      --lang2weight 0.5,1.5 --egs-prefix cegs. 2 \
      $src_dir0/egs_for_multitask $src_dir1/egs_for_multitask $dir/egs_multi
  fi
  common_egs_dir=$dir/egs_multi
fi

if [ $stage -le 17 ]; then
  mkdir -p $dir
  echo "$0: creating neural net configs using the xconfig parser";

  num_targets0=$(tree-info $src_dir0/tree |grep num-pdfs|awk '{print $2}')
  num_targets1=$(tree-info $src_dir1/tree |grep num-pdfs|awk '{print $2}')
  learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)

  mkdir -p $dir/configs
  cat <<EOF > $dir/configs/network.xconfig
  input dim=100 name=ivector
  input dim=40 name=input

  # please note that it is important to have input layer with the name=input
  # as the layer immediately preceding the fixed-affine-layer to enable
  # the use of short notation for the descriptor
  fixed-affine-layer name=lda input=Append(-2,-1,0,1,2,ReplaceIndex(ivector, t, 0)) affine-transform-file=$dir/configs/lda.mat

  # the first splicing is moved before the lda layer, so no splicing here
  relu-renorm-layer name=tdnn1 dim=512
  relu-renorm-layer name=tdnn2 dim=512 input=Append(-1,0,1)
  fast-lstmp-layer name=lstm1 cell-dim=512 recurrent-projection-dim=128 non-recurrent-projection-dim=128 delay=-3
  relu-renorm-layer name=tdnn3 dim=512 input=Append(-3,0,3)
  relu-renorm-layer name=tdnn4 dim=512 input=Append(-3,0,3)
  fast-lstmp-layer name=lstm2 cell-dim=512 recurrent-projection-dim=128 non-recurrent-projection-dim=128 delay=-3
  relu-renorm-layer name=tdnn5 dim=512 input=Append(-3,0,3)
  relu-renorm-layer name=tdnn6 dim=512 input=Append(-3,0,3)
  fast-lstmp-layer name=lstm3 cell-dim=512 recurrent-projection-dim=128 non-recurrent-projection-dim=128 delay=-3

  ## adding the layers for chain branch
  output-layer name=output-0 input=lstm3 output-delay=$label_delay include-log-softmax=false dim=$num_targets0 max-change=1.5
  output-layer name=output-1 input=lstm3 output-delay=$label_delay include-log-softmax=false dim=$num_targets1 max-change=1.5

  # adding the layers for xent branch
  # This block prints the configs for a separate output that will be
  # trained with a cross-entropy objective in the 'chain' models... this
  # has the effect of regularizing the hidden parts of the model.  we use
  # 0.5 / args.xent_regularize as the learning rate factor- the factor of
  # 0.5 / args.xent_regularize is suitable as it means the xent
  # final-layer learns at a rate independent of the regularization
  # constant; and the 0.5 was tuned so as to make the relative progress
  # similar in the xent and regular final layers.
  output-layer name=output-0-xent input=lstm3 output-delay=$label_delay dim=$num_targets0 learning-rate-factor=$learning_rate_factor max-change=1.5
  output-layer name=output-1-xent input=lstm3 output-delay=$label_delay dim=$num_targets1 learning-rate-factor=$learning_rate_factor max-change=1.5
EOF

  cat <<EOF > $dir/temp_edits.config
  remove-output-nodes name=output
  rename-node old-name=output-1 new-name=output
  remove-orphan-nodes
EOF
  steps/nnet3/xconfig_to_configs.py --xconfig-file $dir/configs/network.xconfig \
    --nnet-edits $dir/temp_edits.config \
    --config-dir $dir/configs/

  cp $src_dir0/den.fst $dir/den-output-0.fst
  cp $src_dir1/den.fst $dir/den-output-1.fst
fi

if [ $stage -le 18 ]; then
  if [[ $(hostname -f) == *.clsp.jhu.edu ]] && [ ! -d $dir/egs/storage ]; then
    utils/create_split_dir.pl \
     /export/b0{5,6,7,8}/$USER/kaldi-data/egs/ami-$(date +'%m_%d_%H_%M')/s5/$dir/egs/storage $dir/egs/storage
  fi
 steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$decode_cmd" \
    --feat.online-ivector-dir $train_ivector_dir \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize 0.1 \
    --chain.leaky-hmm-coefficient 0.1 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" \
    --chain.den-fst-to-output="den-output-0.fst:output-0 den-output-1.fst:output-1" \
    --egs.dir "$common_egs_dir" \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-width "$frames_per_chunk" \
    --egs.chunk-left-context "$chunk_left_context" \
    --egs.chunk-right-context "$chunk_right_context" \
    --trainer.num-chunk-per-minibatch 128 \
    --trainer.frames-per-iter 1500000 \
    --trainer.max-param-change 2.0 \
    --trainer.num-epochs 4 \
    --trainer.deriv-truncate-margin 10 \
    --trainer.optimization.shrink-value 0.99 \
    --trainer.optimization.num-jobs-initial 2 \
    --trainer.optimization.num-jobs-final 3 \
    --trainer.optimization.initial-effective-lrate 0.001 \
    --trainer.optimization.final-effective-lrate 0.0001 \
    --trainer.optimization.momentum 0.0 \
    --cleanup.remove-egs false \
    --feat-dir $train_data_dir \
    --tree-dir $tree_dir \
    --lat-dir $lat_dir \
    --dir $dir
fi

if [ $stage -le 19 ]; then
  mkdir -p $dir/output-0

  nnet3-copy --edits="remove-output-nodes name=output; rename-node old-name=output-0 new-name=output" $dir/final.mdl - | \
    nnet3-am-copy --set-raw-nnet=- \
    $src_dir0/final.mdl $dir/output-0/final.mdl || exit 1
  cp $src_dir0/tree $dir/output-0
  cp $dir/{*_opts,*.mat,frame_subsampling_factor} $dir/output-0

  # Note: it might appear that this data/lang_chain directory is mismatched, and it is as
  # far as the 'topo' is concerned, but this script doesn't read the 'topo' from
  # the lang directory.
  utils/mkgraph.sh --self-loop-scale 1.0 \
    data/lang_1200h_test $dir/output-0 $dir/output-0/graph_1200h
fi

for dev in dev_non_overlap_spkr30sec; do
  ivectors_dir=`dirname $extractor`
  if [ $stage -le 21 ]; then
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj 40 \
      data/${dev}_hires $extractor $ivectors_dir/ivectors_$dev
  fi

  if [ $stage -le 22 ]; then
    steps/nnet3/decode.sh --num-threads 4 --nj $decode_nj --cmd "$decode_cmd" \
      --acwt 1.0 --post-decode-acwt 10.0 \
      --extra-left-context $extra_left_context  \
      --extra-right-context $extra_right_context  \
      --frames-per-chunk "$frames_per_chunk" \
      --online-ivector-dir $ivectors_dir/ivectors_$dev \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" \
      $dir/output-0/graph_1200h data/${dev}_hires $dir/output-0/decode_1200h_$dev || exit 1;
  fi

  if [ $stage -le 23 ]; then
    steps/lmrescore_const_arpa.sh --cmd "$decode_cmd" \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" \
      data/lang_1200h_test data/lang_test_fg \
      data/${dev}_hires $dir/output-0/decode_1200h_${dev}{,_fg} || exit 1
  fi
done

if [ $stage -le 24 ]; then
  mkdir -p $dir/output-1

  nnet3-copy --edits="remove-output-nodes name=output; rename-node old-name=output-1 new-name=output" $dir/final.mdl - | \
    nnet3-am-copy --set-raw-nnet=- \
    $src_dir1/final.mdl $dir/output-1/final.mdl || exit 1
  cp $src_dir1/tree $dir/output-1
  cp $dir/{*_opts,*.mat,frame_subsampling_factor} $dir/output-1

  # Note: it might appear that this data/lang_chain directory is mismatched, and it is as
  # far as the 'topo' is concerned, but this script doesn't read the 'topo' from
  # the lang directory.
  utils/mkgraph.sh --self-loop-scale 1.0 \
    data/lang_1200h_test $dir/output-1 $dir/output-1/graph_1200h
fi

for dev in mgb3_dev_Mohamed_spk30sec; do
  ivectors_dir=`dirname $extractor`
  if [ ! -f $ivectors_dir/ivectors_$dev/online_ivectors.scp ]; then
    if [ $stage -le 25 ]; then
      steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj 40 \
        data/${dev}_hires $extractor $ivectors_dir/ivectors_$dev
    fi
  fi

  if [ $stage -le 26 ]; then
    steps/nnet3/decode.sh --num-threads 4 --nj $decode_nj --cmd "$decode_cmd" \
      --acwt 1.0 --post-decode-acwt 10.0 \
      --extra-left-context $extra_left_context  \
      --extra-right-context $extra_right_context  \
      --frames-per-chunk "$frames_per_chunk" \
      --online-ivector-dir $ivectors_dir/ivectors_$dev \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" \
      $dir/output-1/graph_1200h data/${dev}_hires $dir/output-1/decode_1200h_$dev || exit 1;
  fi

  if [ $stage -le 27 ]; then
    steps/lmrescore_const_arpa.sh --cmd "$decode_cmd" \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" \
      data/lang_1200h_test data/lang_test_fg \
      data/${dev}_hires $dir/output-1/decode_1200h_${dev}{,_fg} || exit 1
  fi
done



exit 0
