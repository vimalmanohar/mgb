#!/bin/bash

#started from tedlium recipe with few edits

# This script is same as 1f, but uses a tree-dir with alignments from 
# both mgb2 and mgb3 data for training phone LM.

set -e -o pipefail

# First the options that are passed through to run_ivector_common.sh
# (some of which are also used in this script directly).
stage=-17
nj=60
decode_nj=30
min_seg_len=1.55
chunk_left_context=40
chunk_right_context=0
label_delay=5
xent_regularize=0.1
train_set=mgb3_adapt_combine
num_threads_ubm=32
nnet3_affix=_segmented_1e_adapt_mgb3      # cleanup affix for nnet3 and chain dirs, e.g. _cleaned
# decode options
extra_left_context=50
extra_right_context=0
frames_per_chunk=150

# The rest are configs specific to this script.  Most of the parameters
# are just hardcoded at this level, in the commands below.
train_stage=-10
tdnn_lstm_affix=1c_disc_1h3  #affix for TDNN-LSTM directory, e.g. "a" or "b", in case we change the configuration.
tree_affix=_combine
common_egs_dir=  # you can set this to use previously dumped egs.
extractor=exp/nnet3_mer80/extractor   # use previously trained extractor

graphs_scp=exp/mer80_mgb3_fused/mgb3_sp_adapt_prefix_graphs/G.scp
gmm_dir=
src_dir=exp/chain_segmented_1e/tdnn_lstm_1c_sp_bi_smbr
src_tree_dir=exp/chain_segmented_1e/tree_bi
primary_lr_factor=0.1
num_epochs=2

# End configuration section.
echo "$0 $@"  # Print the command line for logging

. cmd.sh
. ./path.sh
. ./utils/parse_options.sh


if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

local/nnet3/run_ivector_common_adapt.sh --stage $stage \
                                        --nj $nj \
                                        --min-seg-len $min_seg_len \
                                        --train-set $train_set \
                                        --nnet3-affix "$nnet3_affix" \
                                        --extractor "$extractor"

dir=exp/chain${nnet3_affix}/tdnn_lstm_${tdnn_lstm_affix}_sp_bi
train_data_dir=data/${train_set}_sp_hires_comb
lores_train_data_dir=data/${train_set}_sp_comb
train_ivector_dir=exp/nnet3${nnet3_affix}/ivectors_${train_set}_sp_hires_comb
tree_dir=exp/chain${nnet3_affix}/tree_bi${tree_affix}


for f in $train_data_dir/feats.scp $train_ivector_dir/ivector_online.scp; do
  [ ! -f $f ] && echo "$0: expected file $f to exist" && exit 1
done

if [ ! -z "$gmm_dir" ]; then
  for f in $gmm_dir/final.mdl \
      $lores_train_data_dir/feats.scp; do
    [ ! -f $f ] && echo "$0: expected file $f to exist" && exit 1
  done
  lat_dir=exp/chain${nnet3_affix}/${gmm}_${train_set}_sp_comb_lats${graphs_scp:+_sausage}
  
  if [ $stage -le 15 ]; then
    # Get the alignments as lattices (gives the chain training more freedom).
    # use the same num-jobs as the alignments
    steps/align_basis_fmllr_lats.sh --nj 100 --cmd "$train_cmd" \
      ${graphs_scp:+--graphs-scp "$graphs_scp"} \
      ${lores_train_data_dir} \
      data/lang $gmm_dir $lat_dir
    rm $lat_dir/fsts.*.gz # save space
  fi
  chain_opts=(--chain.alignment-subsampling-factor=3 --chain.left-tolerance=5 --chain.right-tolerance=5)
  build_tree_opts=(--alignment-subsampling-factor 3 --frame-subsampling-factor 1 --acwt 0.1)
else
  lat_dir=${src_dir}_${train_set}_sp_comb_lats${graphs_scp:+_sausage}
  
  if [ $stage -le 15 ]; then
    steps/nnet3/align_lats.sh --nj $decode_nj --cmd "$decode_cmd" \
      ${graphs_scp:+--graphs-scp "$graphs_scp"} \
      --scale-opts "--transition-scale=1.0 --self-loop-scale=1.0" \
      --acoustic-scale 1.0 \
      --extra-left-context $extra_left_context  \
      --extra-right-context $extra_right_context  \
      --frames-per-chunk "$frames_per_chunk" \
      --online-ivector-dir $train_ivector_dir \
      $train_data_dir data/lang_chain $src_dir $lat_dir || exit 1;
  fi
  chain_opts=(--chain.alignment-subsampling-factor=1 --chain.left-tolerance=2 --chain.right-tolerance=2)
  build_tree_opts=(--alignment-subsampling-factor 1 --frame-subsampling-factor 3 --acwt 1.0)
fi

# TODO: Add the tree building stage
if [ ! -f $tree_dir/final.mdl ]; then
  echo "$0: Could not find $tree_dir/final.mdl."
  exit 1;
fi

src_mdl=$src_dir/final.mdl 

if [ $stage -le 17 ]; then
  echo "$0: creating neural net configs using the xconfig parser for";
  echo "extra layers w.r.t source network.";
  num_targets=$(tree-info $tree_dir/tree |grep num-pdfs|awk '{print $2}')
  learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)
  mkdir -p $dir
  mkdir -p $dir/configs
  cat <<EOF > $dir/configs/network.xconfig
  ## adding the layers for chain branch
  output-layer name=output-target input=lstm3.rp output-delay=$label_delay include-log-softmax=false dim=$num_targets max-change=1.5
  output-layer name=output-xent-target input=lstm3.rp output-delay=$label_delay dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5
EOF
  # edits.config contains edits required to train transferred model.
  # e.g. substitute output-node of previous model with new output
  # and removing orphan nodes and components.
  cat <<EOF > $dir/configs/edits.config
  remove-output-nodes name=output
  remove-output-nodes name=output-xent
  rename-node old-name=output-target new-name=output
  rename-node old-name=output-xent-target new-name=output-xent
  remove-orphans
EOF
  steps/nnet3/xconfig_to_configs.py --existing-model $src_mdl \
    --xconfig-file  $dir/configs/network.xconfig  \
    --nnet-edits $dir/configs/edits.config \
    --config-dir $dir/configs/
fi
 
if [ $stage -le 18 ]; then
  mkdir -p $dir/converted_src_tree
  num_jobs=$(cat $src_tree_dir/num_jobs)
  $train_cmd JOB=1:$num_jobs $dir/log/convert_ali.JOB.log \
    gunzip -c $src_tree_dir/ali.JOB.gz \| \
    convert-ali $src_tree_dir/final.mdl $tree_dir/final.mdl $tree_dir/tree ark:- \
    ark:- \| gzip -c '>' $dir/converted_src_tree/ali.JOB.gz
  echo $num_jobs > $dir/converted_src_tree/num_jobs

  $train_cmd $dir/log/make_phone_lm.log \
    chain-est-phone-lm --num-extra-lm-states=2000 --scales=1,1 \
    "ark:gunzip -c $tree_dir/ali.*.gz | ali-to-phones $tree_dir/final.mdl ark:- ark:- |" \
    "ark:gunzip -c $dir/converted_src_tree/ali.*.gz | ali-to-phones $tree_dir/final.mdl ark:- ark:- |" \
    $dir/phone_lm.fst 
fi

if [ $stage -le 19 ]; then
  if [[ $(hostname -f) == *.clsp.jhu.edu ]] && [ ! -d $dir/egs/storage ]; then
    utils/create_split_dir.pl \
     /export/b0{5,6,7,8}/$USER/kaldi-data/egs/ami-$(date +'%m_%d_%H_%M')/s5/$dir/egs/storage $dir/egs/storage
  fi
  echo "$0: set the learning-rate-factor for initial network to be zero."
  
  $train_cmd $dir/log/initialize_network.log \
    nnet3-am-copy --raw=true --edits="set-learning-rate-factor name=* learning-rate-factor=$primary_lr_factor" \
    $src_mdl $dir/init.raw || exit 1;

  steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$decode_cmd" \
    --feat.online-ivector-dir $train_ivector_dir \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.phone-lm-fst=$dir/phone_lm.fst \
    --chain.xent-regularize 0.1 \
    --chain.leaky-hmm-coefficient 0.1 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" ${chain_opts[@]} \
    --egs.dir="$common_egs_dir" \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-width "$frames_per_chunk" \
    --egs.chunk-left-context "$chunk_left_context" \
    --egs.chunk-right-context "$chunk_right_context" \
    --trainer.num-chunk-per-minibatch 128 \
    --trainer.frames-per-iter 1500000 \
    --trainer.max-param-change 2.0 \
    --trainer.num-epochs $num_epochs \
    --trainer.deriv-truncate-margin 10 \
    --trainer.optimization.shrink-value 0.99 \
    --trainer.optimization.num-jobs-initial 2 \
    --trainer.optimization.num-jobs-final 3 \
    --trainer.optimization.initial-effective-lrate 0.001 \
    --trainer.optimization.final-effective-lrate 0.0001 \
    --trainer.optimization.momentum 0.5 \
    --cleanup.remove-egs false \
    --feat-dir $train_data_dir \
    --tree-dir $tree_dir \
    --lat-dir $lat_dir \
    --dir $dir
fi



if [ $stage -le 20 ]; then
  # Note: it might appear that this data/lang_chain directory is mismatched, and it is as
  # far as the 'topo' is concerned, but this script doesn't read the 'topo' from
  # the lang directory.
  utils/mkgraph.sh --left-biphone --self-loop-scale 1.0 data/lang_1200h_test $dir $dir/graph_1200h
fi

ivectors_dir=`dirname $extractor`
for dev in mgb3_dev_Mohamed_spkutt; do
  if [ $stage -le 21 ]; then
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj 40 \
      data/${dev}_hires $extractor $ivectors_dir/ivectors_$dev
  fi

  if [ $stage -le 22 ]; then
    steps/nnet3/decode.sh --num-threads 4 --nj $decode_nj --cmd "$decode_cmd" \
      --acwt 1.0 --post-decode-acwt 10.0 \
      --extra-left-context $extra_left_context  \
      --extra-right-context $extra_right_context  \
      --frames-per-chunk "$frames_per_chunk" \
      --online-ivector-dir $ivectors_dir/ivectors_$dev \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" \
      $dir/graph_1200h data/${dev}_hires $dir/decode_1200h_$dev || exit 1;
  fi

  if [ $stage -le 23 ]; then
    steps/lmrescore_const_arpa.sh --cmd "$decode_cmd" \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" \
      data/lang_1200h_test data/lang_test_fg \
      data/${dev}_hires $dir/decode_1200h_${dev}{,_fg} || exit 1
  fi
done

exit 0



