#! /bin/bash

# Copyright  2017 Vimal Manohar
# Apache 2.0

set -e -o pipefail -u

min_seg_len=1.55
stage=-17
nj=60
decode_nj=30
label_delay=5
xent_regularize=0.1
train_set=mgb3_adapt_combine
nnet3_affix=_segmented_1e_multi_mgb3      # cleanup affix for nnet3 and chain dirs, e.g. _cleaned
# decode options
extra_left_context=50
extra_right_context=0

# The rest are configs specific to this script.  Most of the parameters
# are just hardcoded at this level, in the commands below.
train_stage=-10
src_tree_dir=exp/chain_segmented_1e/tree_bi   # Original tree dir
src_dir=exp/chain_segmented_1e/tdnn_lstm_1c_sp_bi
src_egs_dir=exp/chain_segmented_1e/tdnn_lstm__1b_sp_bi/egs_for_multitask
tgt_egs_dir=exp/chain_segmented_1e_adapt_mgb3/tdnn_lstm_1b_sp_bi/egs
tdnn_lstm_affix=1b2  #affix for TDNN-LSTM directory, e.g. "a" or "b", in case we change the configuration.
common_egs_dir=
extractor=exp/nnet3_mer80/extractor  # use previously trained extractor
tree_affix=_combined
gmm_dir=
get_egs_stage=-2

lattice_lm_scale=0
lattice_prune_beam=
acwt=1.0

primary_lr_factor=0.1

# End configuration section.
echo "$0 $@"  # Print the command line for logging

if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

. cmd.sh
. ./path.sh
. ./utils/parse_options.sh

local/nnet3/run_ivector_common_adapt.sh --stage $stage \
                                        --nj $nj \
                                        --min-seg-len $min_seg_len \
                                        --train-set $train_set \
                                        --nnet3-affix "$nnet3_affix" \
                                        --extractor "$extractor"

dir=exp/chain${nnet3_affix}/tdnn_lstm_${tdnn_lstm_affix}_sp_bi
train_data_dir=data/${train_set}_sp_hires_comb
lores_train_data_dir=data/${train_set}_sp_comb
train_ivector_dir=exp/nnet3${nnet3_affix}/ivectors_${train_set}_sp_hires_comb
dir=exp/chain${nnet3_affix}/tdnn_lstm_${tdnn_lstm_affix}_sp_bi
tree_dir=exp/chain${nnet3_affix}/tree_bi${tree_affix}

frames_per_chunk=$(cat $src_egs_dir/info/frames_per_eg)
left_context=$(cat $src_egs_dir/info/left_context)
right_context=$(cat $src_egs_dir/info/right_context)
left_context_initial=$(cat $src_egs_dir/info/left_context_initial)
right_context_final=$(cat $src_egs_dir/info/right_context_final)
cmvn_opts=$(cat $src_egs_dir/cmvn_opts)

for f in $train_data_dir/feats.scp $train_ivector_dir/ivector_online.scp; do
  [ ! -f $f ] && echo "$0: expected file $f to exist" && exit 1
done

if [ ! -z "$gmm_dir" ]; then
  for f in $gmm_dir/final.mdl \
      $lores_train_data_dir/feats.scp; do
    [ ! -f $f ] && echo "$0: expected file $f to exist" && exit 1
  done
  lat_dir=exp/chain${nnet3_affix}/${gmm}_${train_set}_sp_comb_lats${graphs_scp:+_sausage}

  if [ $stage -le 15 ]; then
    if [ -f $lat_dir/lat.1.gz ]; then
      echo "$lat_dir/lat.1.gz exists. Delete and try again" && exit 1
    fi
  
    # Get the alignments as lattices (gives the chain training more freedom).
    # use the same num-jobs as the alignments
    steps/align_basis_fmllr_lats.sh --nj 100 --cmd "$train_cmd" \
      ${graphs_scp:+--graphs-scp "$graphs_scp"} \
      ${lores_train_data_dir} \
      data/lang $gmm_dir $lat_dir
    rm $lat_dir/fsts.*.gz # save space
  fi
  chain_opts=(--chain.alignment-subsampling-factor=3 --chain.left-tolerance=5 --chain.right-tolerance=5)
  build_tree_opts=(--alignment-subsampling-factor 3 --frame-subsampling-factor 1 --acwt 0.1)
  egs_opts="--alignment-subsampling-factor 3 --left-tolerance 5 --right-tolerance 5"
else
  lat_dir=${src_dir}_${train_set}_sp_comb_lats${graphs_scp:+_sausage}
  
  if [ $stage -le 15 ]; then
    if [ -f $lat_dir/lat.1.gz ]; then
      echo "$lat_dir/lat.1.gz exists. Delete and try again" && exit 1
    fi
  
    steps/nnet3/align_lats.sh --nj $decode_nj --cmd "$decode_cmd" \
      ${graphs_scp:+--graphs-scp "$graphs_scp"} \
      --scale-opts "--transition-scale=1.0 --self-loop-scale=1.0" \
      --acoustic-scale 1.0 \
      --extra-left-context $extra_left_context  \
      --extra-right-context $extra_right_context  \
      --frames-per-chunk "$frames_per_chunk" \
      --online-ivector-dir $train_ivector_dir \
      $train_data_dir data/lang_chain $src_dir $lat_dir || exit 1;
  fi
  chain_opts=(--chain.alignment-subsampling-factor=1 --chain.left-tolerance=2 --chain.right-tolerance=2)
  build_tree_opts=(--alignment-subsampling-factor 1 --frame-subsampling-factor 3 --acwt 1.0)
  egs_opts="--alignment-subsampling-factor 1 --left-tolerance 2 --right-tolerance 2"
fi

if [ $stage -le 16 ]; then
  # Build a tree using our new topology.  We know we have alignments for the
  # speed-perturbed data (local/nnet3/run_ivector_common.sh made them), so use
  # those.
  if [ -f $tree_dir/final.mdl ]; then
    echo "$0: $tree_dir/final.mdl already exists, refusing to overwrite it."
    exit 1;
  fi
  steps/nnet3/chain/build_tree_from_lats.sh ${build_tree_opts[@]} \
      --context-opts "--context-width=2 --central-position=1" \
      --leftmost-questions-truncate -1 \
      --cmd "$train_cmd" 4000 ${train_data_dir} data/lang_chain $lat_dir ${tree_dir}_temp

  cp -rT $src_tree_dir $tree_dir
  src_num_jobs=$(cat $src_tree_dir/num_jobs)
  num_jobs=$(cat ${tree_dir}_tmp/num_jobs)

  $train_cmd JOB=1:$num_jobs $tree_dir/convert_ali.JOB.log \
    convert-ali --frame-subsampling-factor=1 \
    ${tree_dir}_temp/final.mdl ${tree_dir}/final.mdl ${tree_dir}/tree \
    "ark:gunzip -c ${tree_dir}_temp/ali.JOB.gz |" \
    "ark:| gzip -c > ${tree_dir}/ali.\$[$src_num_jobs+JOB].gz" || exit 1

  echo $[src_num_jobs + num_jobs] > $tree_dir/num_jobs
fi

src_mdl=$src_dir/final.mdl 

if [ $stage -le 17 ]; then
  echo "$0: creating neural net configs using the xconfig parser for";
  echo "extra layers w.r.t source network.";
  num_targets=$(tree-info $tree_dir/tree |grep num-pdfs|awk '{print $2}')
  learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)
  mkdir -p $dir
  mkdir -p $dir/configs
  cat <<EOF > $dir/configs/network.xconfig
  ## adding the layers for chain branch
  output-layer name=output-0 input=lstm3.rp output-delay=$label_delay include-log-softmax=false dim=$num_targets max-change=1.5
  output-layer name=output-0-xent input=lstm3.rp output-delay=$label_delay dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5
EOF
  
  cat <<EOF > $dir/temp_edits.config
  remove-output-nodes name=output
  rename-node old-name=output-0 new-name=output
  remove-orphan-nodes
EOF
  steps/nnet3/xconfig_to_configs.py --existing-model $src_mdl \
    --xconfig-file $dir/configs/network.xconfig \
    --nnet-edits $dir/temp_edits.config \
    --config-dir $dir/configs/

  $train_cmd $dir/log/copy_transition_model.log \
    copy-transition-model $tree_dir/final.mdl $dir/0.trans_mdl || exit 1
  $train_cmd $dir/log/make_phone_lm.log \
    gunzip -c $tree_dir/ali.*.gz \| \
    ali-to-phones $tree_dir/final.mdl ark:- ark:- \| \
    chain-est-phone-lm --num-extra-lm-states=2000 ark:- $dir/phone_lm.fst
  $train_cmd $dir/log/make_den_fst.log \
    chain-make-den-fst $tree_dir/tree $tree_dir/final.mdl \
    $dir/phone_lm.fst $dir/den-output-0.fst $dir/normalization.fst || exit 1
  cp $src_dir/den.fst $dir/den-output-1.fst || exit 1
fi

if [ -z "$common_egs_dir" ]; then
  if [ -z "$tgt_egs_dir" ]; then
    cp $tree_dir/tree $dir/tree

    if [ $stage -le 18 ]; then
      steps/nnet3/chain/get_egs.sh --generate-egs-scp true \
        --stage $get_egs_stage $egs_opts --cmd "$train_cmd" \
        --frames-per-eg $frames_per_chunk \
        --left-context $left_context \
        --right-context $right_context \
        --left-context-initial $left_context_initial \
        --right-context-final $right_context_final \
        --frames-per-iter 1500000 \
        --lattice-lm-scale $lattice_lm_scale --lattice-prune-beam "$lattice_prune_beam" \
        --acwt $acwt \
        --online-ivector-dir $train_ivector_dir \
        ${train_data_dir} ${dir} ${lat_dir} ${dir}/egs
    fi
    tgt_egs_dir=$dir/egs
  fi

  if [ $stage -le 19 ]; then
    steps/nnet3/multilingual/combine_egs.sh --cmd "$train_cmd" \
      --allocate-opts "--random-lang=true --get-all-first-lang-only=true" \
      --minibatch-size 128 --samples-per-iter 10000 \
      --lang2weight 1.0,0.1 --egs-prefix cegs. 2 \
      $tgt_egs_dir $src_egs_dir $dir/egs_multi
  fi
  #if [ $stage -le 19 ]; then
  #  comb_egs_dir=$dir/egs_comb
  #  echo "$0: combining supervised/unsupervised egs"
  #  n1=`cat $tgt_egs_dir/info/num_archives`
  #  n2=`cat $src_egs_dir/info/num_archives`
  #  num_archives=$[n1 * 2]
  #  mkdir -p $comb_egs_dir/log
  #  cp {$tgt_egs_dir,$comb_egs_dir}/train_diagnostic.cegs
  #  cp {$tgt_egs_dir,$comb_egs_dir}/valid_diagnostic.cegs
  #  cp {$tgt_egs_dir,$comb_egs_dir}/combine.cegs
  #  cp {$tgt_egs_dir,$comb_egs_dir}/cmvn_opts
  #  cp -r $tgt_egs_dir/info $comb_egs_dir
  #  echo $num_archives > $comb_egs_dir/info/num_archives
  #  cat $tgt_egs_dir/info/num_frames | awk '{print $1 * 2}' > $comb_egs_dir/info/num_frames
  #  cat {$tgt_egs_dir,$src_egs_dir}/info/egs_per_archive | awk '{s+=$1} END{print s/2.0}' > $comb_egs_dir/info/egs_per_archive
  #  out_egs_list=
  #  egs_list=
  #  for n in $(seq $num_archives); do
  #    [ -f $tgt_egs_dir/cegs.$n.ark ] && egs_list="$egs_list $tgt_egs_dir/cegs.$n.ark"
  #    [ -f $src_egs_dir/cegs.$n.ark ] && egs_list="$egs_list $src_egs_dir/cegs.$n.ark"
  #    out_egs_list="$out_egs_list ark:$comb_egs_dir/cegs.$[2*n].ark ark:$comb_egs_dir/cegs.$[2*n+1].ark"
  #  done
  #  srand=0
  #  $decode_cmd $comb_egs_dir/log/combine.log \
  #    nnet3-chain-copy-egs "ark:cat $egs_list |" $out_egs_list
  #fi
  common_egs_dir=$dir/egs_multi
fi

if [ $stage -le 20 ]; then
  if [[ $(hostname -f) == *.clsp.jhu.edu ]] && [ ! -d $dir/egs/storage ]; then
    utils/create_split_dir.pl \
     /export/b0{5,6,7,8}/$USER/kaldi-data/egs/ami-$(date +'%m_%d_%H_%M')/s5/$dir/egs/storage $dir/egs/storage
  fi
  
  echo "$0: set the learning-rate-factor for initial network to be $primary_lr_factor"
  nnet3-copy --edits="set-learning-rate-factor name=* learning-rate-factor=$primary_lr_factor" \
    $src_mdl $dir/init.raw || exit 1;

  # First modify the network to add a new output node
  cat <<EOF > $dir/configs/modify_network.config
output-node name=output-temp input=Offset(output.affine, 5) objective=linear
EOF
  
  # edits.config contains edits required to train transferred model.
  # e.g. substitute output-node of previous model with new output
  # and removing orphan nodes and components.
  cat <<EOF > $dir/configs/edits.config
  rename-node old-name=output new-name=output-1
  rename-node old-name=output-xent new-name=output-1-xent
  rename-node old-name=output-temp new-name=output   # required to feign a AmSimpleNnet
  remove-orphans
EOF

 steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$decode_cmd" \
    --feat.online-ivector-dir $train_ivector_dir \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize 0.1 \
    --chain.leaky-hmm-coefficient 0.1 \
    --chain.l2-regularize 0.00005 \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" \
    --chain.den-fst-to-output="den-output-0.fst:output-0 den-output-1.fst:output-1" \
    --egs.dir "$common_egs_dir" \
    --egs.opts "--frames-overlap-per-eg 0" \
    --egs.chunk-left-context=40 \
    --egs.chunk-right-context=0 \
    --egs.chunk-width=150 \
    --trainer.num-chunk-per-minibatch 128 \
    --trainer.frames-per-iter 1500000 \
    --trainer.max-param-change 2.0 \
    --trainer.num-epochs 4 \
    --trainer.deriv-truncate-margin 10 \
    --trainer.optimization.shrink-value 0.99 \
    --trainer.optimization.num-jobs-initial 2 \
    --trainer.optimization.num-jobs-final 3 \
    --trainer.optimization.initial-effective-lrate 0.001 \
    --trainer.optimization.final-effective-lrate 0.0001 \
    --trainer.optimization.momentum 0.0 \
    --cleanup.remove-egs false \
    --feat-dir $train_data_dir \
    --tree-dir $tree_dir \
    --lat-dir $lat_dir \
    --dir $dir
fi

if [ $stage -le 21 ]; then
  mkdir -p $dir/output-0

  nnet3-copy --edits="remove-output-nodes name=output; rename-node old-name=output-0 new-name=output" $dir/final.mdl - | \
    nnet3-am-copy --set-raw-nnet=- \
    $dir/final.mdl $dir/output-0/final.mdl || exit 1
  cp $dir/tree $dir/output-0
  cp $dir/{*_opts,*.mat,frame_subsampling_factor} $dir/output-0 || true

  # Note: it might appear that this data/lang_chain directory is mismatched, and it is as
  # far as the 'topo' is concerned, but this script doesn't read the 'topo' from
  # the lang directory.
  utils/mkgraph.sh --self-loop-scale 1.0 \
    data/lang_1200h_test $dir/output-0 $dir/output-0/graph_1200h
fi

for dev in mgb3_dev_Mohamed_spk30sec; do
  ivectors_dir=`dirname $extractor`
  if [ $stage -le 22 ]; then
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj 40 \
      data/${dev}_hires $extractor $ivectors_dir/ivectors_$dev
  fi

  if [ $stage -le 23 ]; then
    steps/nnet3/decode.sh --num-threads 4 --nj $decode_nj --cmd "$decode_cmd" \
      --acwt 1.0 --post-decode-acwt 10.0 \
      --extra-left-context $extra_left_context  \
      --extra-right-context $extra_right_context  \
      --frames-per-chunk "$frames_per_chunk" \
      --online-ivector-dir $ivectors_dir/ivectors_$dev \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" \
      $dir/output-0/graph_1200h data/${dev}_hires $dir/output-0/decode_1200h_$dev || exit 1;
  fi

  if [ $stage -le 24 ]; then
    steps/lmrescore_const_arpa.sh --cmd "$decode_cmd" \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" \
      data/lang_1200h_test data/lang_test_fg \
      data/${dev}_hires $dir/output-0/decode_1200h_${dev}{,_fg} || exit 1
  fi
done

exit 0
if [ $stage -le 24 ]; then
  mkdir -p $dir/output-1

  nnet3-copy --edits="remove-output-nodes name=output; rename-node old-name=output-1 new-name=output" $dir/final.mdl - | \
    nnet3-am-copy --set-raw-nnet=- \
    $src_dir1/final.mdl $dir/output-1/final.mdl || exit 1


  cp $src_dir1/tree $dir/output-1
  cp $dir/{*_opts,*.mat,frame_subsampling_factor} $dir/output-1

  # Note: it might appear that this data/lang_chain directory is mismatched, and it is as
  # far as the 'topo' is concerned, but this script doesn't read the 'topo' from
  # the lang directory.
  utils/mkgraph.sh --self-loop-scale 1.0 \
    data/lang_1200h_test $dir/output-1 $dir/output-1/graph_1200h
fi

for dev in mgb3_dev_Mohamed_spk30sec; do
  ivectors_dir=`dirname $extractor`
  if [ ! -f $ivectors_dir/ivectors_$dev/online_ivectors.scp ]; then
    if [ $stage -le 25 ]; then
      steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj 40 \
        data/${dev}_hires $extractor $ivectors_dir/ivectors_$dev
    fi
  fi

  if [ $stage -le 26 ]; then
    steps/nnet3/decode.sh --num-threads 4 --nj $decode_nj --cmd "$decode_cmd" \
      --acwt 1.0 --post-decode-acwt 10.0 \
      --extra-left-context $extra_left_context  \
      --extra-right-context $extra_right_context  \
      --frames-per-chunk "$frames_per_chunk" \
      --online-ivector-dir $ivectors_dir/ivectors_$dev \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" \
      $dir/output-1/graph_1200h data/${dev}_hires $dir/output-1/decode_1200h_$dev || exit 1;
  fi

  if [ $stage -le 27 ]; then
    steps/lmrescore_const_arpa.sh --cmd "$decode_cmd" \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" \
      data/lang_1200h_test data/lang_test_fg \
      data/${dev}_hires $dir/output-1/decode_1200h_${dev}{,_fg} || exit 1
  fi
done



exit 0
