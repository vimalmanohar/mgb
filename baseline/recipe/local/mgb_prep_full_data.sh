#!/bin/bash

# Copyright (C) 2016, Qatar Computing Research Institute, HBKU
#               2016  Vimal Manohar

if [ $# -ne 3 ]; then
  echo "Usage: $0 <wav-dir> <xml-dir> <mer-sel>"
  exit 1;
fi

wavDir=$1
xmldir=$2
mer=$3

trainDir=data/train_1200h_mer$mer

rm -r $trainDir || true

for file in train; do
  if [ ! -f $file ]; then
    echo "$0: no such file $file - copy $file from GitHub repository ArabicASRChallenge2016/download/"
    exit 1;
  fi
done 

set -e -o pipefail

mkdir -p $trainDir
cut -d '/' -f2 train | while read basename; do     
    [ ! -e $xmldir/$basename.xml ] && echo "Missing $xmldir/$basename.xml" && exit 1
    $XMLSTARLET/xml sel -t -m '//segments[@annotation_id="transcript_align"]' -m "segment" -n -v  "concat(@who,' ',@starttime,' ',@endtime,' ',@WMER,' ')" -m "element" -v "concat(text(),' ')" $xmldir/$basename.xml | local/add_to_datadir.py $basename $trainDir $mer
    echo $basename $wavDir/$basename.wav >> $trainDir/wav.scp
done

utils/fix_data_dir.sh $trainDir
utils/validate_data_dir.sh --no-feats $trainDir
