#!/bin/bash
# Copyright 2012  Johns Hopkins University (Author: Daniel Povey)
# Apache 2.0

[ -f ./path.sh ] && . ./path.sh

# begin configuration section.
cmd=run.pl
stage=0
decode_mbr=true
reverse=false
word_ins_penalty=0.0
min_lmwt=9
max_lmwt=30
iter=final

#end configuration section.

[ -f ./path.sh ] && . ./path.sh
. parse_options.sh || exit 1;

if [ $# -ne 3 ]; then
  echo "Usage: local/score.sh [--cmd (run.pl|queue.pl...)] <data-dir> <lang-dir|graph-dir> <decode-dir>"
  echo " Options:"
  echo "    --cmd (run.pl|queue.pl...)      # specify how to run the sub-processes."
  echo "    --stage (0|1|2)                 # start scoring script from part-way through."
  echo "    --decode_mbr (true/false)       # maximum bayes risk decoding (confusion network)."
  echo "    --min_lmwt <int>                # minumum LM-weight for lattice rescoring "
  echo "    --max_lmwt <int>                # maximum LM-weight for lattice rescoring "
  echo "    --reverse (true/false)          # score with time reversed features "
  exit 1;
fi

data=$1
lang_or_graph=$2
dir=$3
srcdir=`dirname $dir`;
symtab=$lang_or_graph/words.txt

for f in $symtab $dir/lat.1.gz $data/text; do
  [ ! -f $f ] && echo "score.sh: no such file $f" && exit 1;
done

name=`basename $data`

mkdir -p $dir/scoring_mrwer/log

#cat $data/text | sed 's:<NOISE>::g' | sed 's:<SPOKEN_NOISE>::g' > $dir/scoring_mrwer/test_filt.txt
if [ $stage -le 0 ]; then
  $cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring_mrwer/log/best_path.LMWT.log \
    mkdir -p $dir/score_mrwer_LMWT/ '&&' \
    lattice-scale --inv-acoustic-scale=LMWT "ark:gunzip -c $dir/lat.*.gz|" ark:- \| \
    lattice-best-path --word-symbol-table=$symtab ark:- ark,t:- \| \
    utils/int2sym.pl -f 2- $symtab '>' $dir/score_mrwer_LMWT/${name}.txt || exit 1
fi    

## Remove some stuff we don't want to score, from the ctm.                                                                                                   
#if [ $stage -le 1 ]; then 
#  for x in $dir/score_mrwer_*/$name.uttctm.updated; do
#    cp $x $dir/tmpf;
#    cat $dir/tmpf | grep -v -E '\[NOISE|LAUGHTER|VOCALIZED-NOISE\]' | \
#      grep -v -E '<UNK>|%HESITATION' > $x;
#  done
#fi  

function normalize {
  inFile=$1
  outFile=$2
  cut -d ' ' -f2- $inFile | \
    perl -pe 's/[><|]/A/g;s/p/h/g;s/Y/y/g;' | \
    paste -d ' ' <(cut -d ' ' -f1 $inFile) - > $outFile
}

refs=$(cat $data/mrwer_refs | tr '\n' ' ')
num_refs=$(cat $data/mrwer_refs | wc -l)

# we will use the data where all transcribers marked as non-overlap speech    
awk '{print $1}' $refs | sort | uniq -c  | grep " $num_refs "  | awk '{print $2}' | sort -u > $dir/scoring_mrwer/id$$

rm $dir/scoring_mrwer/*.common $dir/scoring_mrwer/*.common.norm

i=1
for x in $refs; do
  grep -f $dir/scoring_mrwer/id$$ $x > \
    $dir/scoring_mrwer/`basename $x`_${i}.common &
  i=$[i+1]
done
wait
  
i=1
normalized_refs=()
for x in $refs; do
  normalize $dir/scoring_mrwer/`basename $x`_${i}.common $dir/scoring_mrwer/`basename $x`_${i}.common.norm
  normalized_refs+=($dir/scoring_mrwer/`basename $x`_${i}.common.norm)
  rm $dir/scoring_mrwer/`basename $x`_${i}.common
  i=$[i+1]
done

for lmwt in `seq $min_lmwt $max_lmwt`; do
  grep -f $dir/scoring_mrwer/id$$ $dir/score_mrwer_$lmwt/${name}.txt > \
    $dir/score_mrwer_$lmwt/${name}.txt.common &
done
wait

for lmwt in `seq $min_lmwt $max_lmwt`; do
  normalize \
    $dir/score_mrwer_$lmwt/${name}.txt.common \
    $dir/score_mrwer_$lmwt/${name}.txt.common.norm
done

rm $dir/scoring_mrwer/id$$ $dir/scoring_mrwer/*.common

if [ $stage -le 2 ]; then
  $cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring_mrwer/log/score.LMWT.log \
    local/mrwer/mrwer.py -e "${normalized_refs[@]}" \
    $dir/score_mrwer_LMWT/${name}.txt.common.norm || exit 1;
fi 
