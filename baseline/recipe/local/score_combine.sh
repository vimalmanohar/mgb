#!/bin/bash

# Copyright 2013  Arnab Ghoshal

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.


# Script for system combination using minimum Bayes risk decoding.
# This calls lattice-combine to create a union of lattices that have been 
# normalized by removing the total forward cost from them. The resulting lattice
# is used as input to lattice-mbr-decode. This should not be put in steps/ or 
# utils/ since the scores on the combined lattice must not be scaled.

# begin configuration section.
cmd=run.pl
min_lmwt=9
max_lmwt=20
lat_weights=
stage=-1
#end configuration section.

help_message="Usage: "$(basename $0)" [options] <data-dir> <graph-dir|lang-dir> <decode-dir1> <decode-dir2> [decode-dir3 ... ] <out-dir>
Options:
  --cmd (run.pl|queue.pl...)      # specify how to run the sub-processes.
  --min-lmwt INT                  # minumum LM-weight for lattice rescoring 
  --max-lmwt INT                  # maximum LM-weight for lattice rescoring
  --lat-weights STR               # colon-separated string of lattice weights
";

[ -f ./path.sh ] && . ./path.sh
. parse_options.sh || exit 1;

if [ $# -lt 5 ]; then
  printf "$help_message\n";
  exit 1;
fi

data=$1
graphdir=$2
odir=${@: -1}  # last argument to the script
shift 2;
decode_dirs=( $@ )  # read the remaining arguments into an array
unset decode_dirs[${#decode_dirs[@]}-1]  # 'pop' the last argument which is odir
num_sys=${#decode_dirs[@]}  # number of systems to combine

symtab=$graphdir/words.txt
[ ! -f $symtab ] && echo "$0: missing word symbol table '$symtab'" && exit 1;

name=$(basename $data)

mkdir -p $odir/log

for i in `seq 0 $[num_sys-1]`; do
  model=${decode_dirs[$i]}/../final.mdl  # model one level up from decode dir
  for f in $model ${decode_dirs[$i]}/lat.1.gz ; do
    [ ! -f $f ] && echo "$0: expecting file $f to exist" && exit 1;
  done
  lats[$i]="\"ark:gunzip -c ${decode_dirs[$i]}/lat.*.gz |\""
done


dir=${decode_dirs[0]}
if [ -f $dir/../frame_shift ]; then
  frame_shift_opt="--frame-shift=$(cat $dir/../frame_shift)"
  echo "$0: $dir/../frame_shift exists, using $frame_shift_opt"
elif [ -f $dir/../frame_subsampling_factor ]; then
  factor=$(cat $dir/../frame_subsampling_factor) || exit 1
  frame_shift_opt="--frame-shift=0.0$factor"
  echo "$0: $dir/../frame_subsampling_factor exists, using $frame_shift_opt"
fi

mkdir -p $odir/scoring/log

function normalize {
  inFile=$1
  outFile=$2
  cut -d ' ' -f2- $inFile | \
    perl -pe 's/[><|]/A/g;s/p/h/g;s/Y/y/g;' | \
    paste -d ' ' <(cut -d ' ' -f1 $inFile) - > $outFile
}

if [ ! -f $data/mrwer_refs ]; then
  if [ $stage -le 1 ]; then
    $cmd LMWT=$min_lmwt:$max_lmwt $odir/log/combine_lats.LMWT.log \
      mkdir -p $odir/score_LMWT '&&' \
      lattice-combine --inv-acoustic-scale=LMWT ${lat_weights:+--lat-weights=$lat_weights} ${lats[@]} ark:- \| \
      lattice-to-ctm-conf $frame_shift_opt --decode-mbr=true ark:- - \| \
      utils/int2sym.pl -f 5 $symtab \| \
      utils/convert_ctm.pl $data/segments $data/reco2file_and_channel \
      '>' $odir/score_LMWT/${name}.ctm '&&' \
      grep -v '\<UNK\>' $odir/score_LMWT/${name}.ctm \| \
      sed -e 's:^[^ ]*\/::' -e 's:.wav::' \| sort -k1,1 -k3,3n \
      '>' $odir/score_LMWT/${name}.ctm.updated || exit 1;
    
    for x in $odir/score_*/$name.ctm.updated; do
      cp $x $odir/tmpf;
      cat $odir/tmpf | grep -v -E '\[NOISE|LAUGHTER|VOCALIZED-NOISE\]' | \
        grep -v -E '<UNK>|%HESITATION' > $x;
    done
  fi
else
  if [ $stage -le 1 ]; then
    $cmd LMWT=$min_lmwt:$max_lmwt $odir/scoring_mrwer/log/combine_lats.LMWT.log \
      mkdir -p $odir/score_mrwer_LMWT '&&' \
      lattice-combine --inv-acoustic-scale=LMWT ${lat_weights:+--lat-weights=$lat_weights} ${lats[@]} ark:- \| \
      lattice-mbr-decode ark:- ark,t:- \| \
      utils/int2sym.pl -f 2- $symtab \
      '>' $odir/score_mrwer_LMWT/${name}.txt || exit 1
  fi

  dir=$odir
  refs=$(cat $data/mrwer_refs | tr '\n' ' ')
  num_refs=$(cat $data/mrwer_refs | wc -l)

  # we will use the data where all transcribers marked as non-overlap speech    
  awk '{print $1}' $refs | sort | uniq -c  | grep " $num_refs "  | awk '{print $2}' | sort -u > $dir/scoring_mrwer/id$$

  rm $dir/scoring_mrwer/*.common $dir/scoring_mrwer/*.common.norm

  i=1
  for x in $refs; do
    grep -f $dir/scoring_mrwer/id$$ $x > \
      $dir/scoring_mrwer/`basename $x`_${i}.common &
    i=$[i+1]
  done
  wait

  i=1
  normalized_refs=()
  for x in $refs; do
    normalize $dir/scoring_mrwer/`basename $x`_${i}.common $dir/scoring_mrwer/`basename $x`_${i}.common.norm
    normalized_refs+=($dir/scoring_mrwer/`basename $x`_${i}.common.norm)
    rm $dir/scoring_mrwer/`basename $x`_${i}.common
    i=$[i+1]
  done

  for lmwt in `seq $min_lmwt $max_lmwt`; do
    grep -f $dir/scoring_mrwer/id$$ $dir/score_mrwer_$lmwt/${name}.txt > \
      $dir/score_mrwer_$lmwt/${name}.txt.common &
  done
  wait

  for lmwt in `seq $min_lmwt $max_lmwt`; do
    normalize \
      $dir/score_mrwer_$lmwt/${name}.txt.common \
      $dir/score_mrwer_$lmwt/${name}.txt.common.norm
  done

  rm $dir/scoring_mrwer/id$$ $dir/scoring_mrwer/*.common

  if [ $stage -le 2 ]; then
    $cmd LMWT=$min_lmwt:$max_lmwt $dir/scoring_mrwer/log/score.LMWT.log \
      local/mrwer/mrwer.py -e "${normalized_refs[@]}" \
      $dir/score_mrwer_LMWT/${name}.txt.common.norm || exit 1;
  fi 
fi

exit 0
