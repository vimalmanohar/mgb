#! /bin/bash

# Copyright 2017
# Vimal Manohar

set -e -o pipefail -u

ROOT_DIR=/export/a15/vmanoha1/MGB/test.20170515
extractor=exp/nnet3_mer80/extractor   # must match training

chain_dir=exp/chain_segmented_1e_adapt_mgb3/tdnn_lstm_1c_disc_1g_sp_bi

graph_dir=    # If not provided, $chain_dir/graph_1200h will be used

extra_left_context=50
extra_right_context=0
frames_per_chunk=150
skip_scoring=false

lang_test=data/lang_1200h_test   # For creating lattices

lang_rescore=data/lang_test_fg   # For rescoring
rescore_suffix=fg 

result_affix=contrastive1

. path.sh
. cmd.sh 
. utils/parse_options.sh

if [ -z "$graph_dir" ]; then
  graph_dir=$chain_dir/graph_1200h
fi

if [ $# -ne 0 ]; then
  echo "Usage: $0"
  exit 1
fi

if [ ! -f data/mgb3_test/.done ]; then
  local/mgb3_data_prep.sh $ROOT_DIR/wav $ROOT_DIR/Mohamed data/mgb3_test
  touch data/mgb3_test/.done
fi

if [ ! -f data/mgb3_test/.mfcc.done ]; then
  steps/make_mfcc.sh --nj 40 --cmd "$train_cmd" data/mgb3_test
  steps/compute_cmvn_stats.sh data/mgb3_test
  touch data/mgb3_test/.mfcc.done
fi

if [ ! -f data/mgb3_test_spk30sec/.done ]; then
  utils/data/modify_speaker_info.sh --seconds-per-spk-max 30 \
    --respect-speaker-info true \
    data/mgb3_test data/mgb3_test_spk30sec
  steps/compute_cmvn_stats.sh data/mgb3_test_spk30sec
  touch data/mgb3_test_spk30sec/.done
fi

if [ ! -f data/mgb3_test_hires/.done ]; then
  utils/copy_data_dir.sh data/mgb3_test data/mgb3_test_hires
  steps/make_mfcc.sh --mfcc-config conf/mfcc_hires.conf \
    --nj 40 --cmd "$train_cmd" data/mgb3_test_hires
  touch data/mgb3_test_hires/.done
fi

if [ ! -f data/mgb3_test_spk30sec_hires/.done ]; then
  utils/data/modify_speaker_info.sh --seconds-per-spk-max 30 \
    --respect-speaker-info true \
    data/mgb3_test_hires data/mgb3_test_spk30sec_hires
  steps/compute_cmvn_stats.sh data/mgb3_test_spk30sec_hires
  touch data/mgb3_test_spk30sec_hires/.done
fi

if [ ! -f data/mgb3_test_spkutt_hires/.done ]; then
  utils/data/modify_speaker_info.sh --utts-per-spk-max 1 \
    data/mgb3_test_hires data/mgb3_test_spkutt_hires
  steps/compute_cmvn_stats.sh data/mgb3_test_spkutt_hires
  touch data/mgb3_test_spkutt_hires/.done
fi

for test in mgb3_test_spkutt; do
  ivectors_dir=`dirname $extractor`_ivectors_${test}

  if [ ! -f $ivectors_dir/.done ]; then
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj 40 \
      data/${test}_hires $extractor $ivectors_dir
    touch $ivectors_dir/.done
  fi
  graph_suffix=${graph_dir#*graph}

  decode_dir=$chain_dir/decode${graph_suffix}_$test

  if [ ! -f $decode_dir/.done ]; then
    steps/nnet3/decode.sh --num-threads 4 --nj 40 --cmd "$decode_cmd" \
      --acwt 1.0 --post-decode-acwt 10.0 \
      --extra-left-context $extra_left_context  \
      --extra-right-context $extra_right_context  \
      --frames-per-chunk "$frames_per_chunk" \
      --online-ivector-dir $ivectors_dir --skip-scoring $skip_scoring \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" \
      $graph_dir data/${test}_hires $decode_dir || exit 1
    touch $decode_dir/.done
  fi

  if [ ! -f ${decode_dir}_${rescore_suffix}/.done ]; then
    steps/lmrescore_const_arpa.sh --cmd "$decode_cmd" \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" --skip-scoring $skip_scoring \
      $lang_test $lang_rescore \
      data/${test}_hires ${decode_dir}{,_$rescore_suffix} || exit 1
    touch ${decode_dir}_${rescore_suffix}/.done
  fi

  local/lattice_to_ctm.sh --min-lmwt 5 --max-lmwt 15 \
    data/${test}_hires $lang_rescore ${decode_dir}_$rescore_suffix 
  (cd ${decode_dir}_$rescore_suffix/score_10/; tar -czvf task1a.${result_affix}.ctm.tgz ${test}_hires.ctm.updated)
  echo `readlink -f ${decode_dir}_$rescore_suffix/score_10/task1a.${result_affix}.ctm.tgz`
done
