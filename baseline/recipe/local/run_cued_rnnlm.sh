#!/bin/bash

mer=80
crit=vr
n=50
ngram_order=4
nwords=200000
hidden=200

. ./utils/parse_options.sh
. ./cmd.sh
. ./path.sh

set -e

[ -z "$LM_DIR" ] && echo "LM_DIR must be defined." && exit 1
lm_text=$LM_DIR/mgb.arabic.normalized.bukwalter
text=data/train_1200h_mer$mer/text

mkdir -p data/local/cued_rnn_$crit

[ ! -f $text ] && echo "$0: No such file $text" && exit 1;
awk 'BEGIN{i=0} {i++; print "foo-"i" "$0;}' $lm_text | cat - $text > data/local/cued_rnn_$crit/text

nwords_suffix=$(perl -e "print int($nwords / 1000)")
local/train_cued_rnnlms.sh --crit $crit --nwords $nwords --hidden $hidden --train-text data/local/cued_rnn_${crit}/text \
  data/cued_rnn_${crit}_n${nwords_suffix}k_h${hidden}

#final_lm=ami_fsh.o3g.kn
#LM=$final_lm.pr1-7
#
#for decode_set in dev eval; do
#  dir=exp/$mic/nnet3/tdnn_sp/
#  decode_dir=${dir}/decode_${decode_set}
#
#  # N-best rescoring
#  steps/rnnlmrescore.sh \
#    --rnnlm-ver cuedrnnlm \
#    --N $n --cmd "$decode_cmd --mem 16G" --inv-acwt 10 0.5 \
#    data/lang_$LM data/$mic/cued_rnn_$crit \
#    data/$mic/$decode_set ${decode_dir} \
#    ${decode_dir}.rnnlm.$crit.cued.$n-best 
#
#  # Lattice rescoring
#  steps/lmrescore_rnnlm_lat.sh \
#    --cmd "$decode_cmd --mem 16G" \
#    --rnnlm-ver cuedrnnlm  --weight 0.5 --max-ngram-order $ngram_order \
#    data/lang_$LM data/$mic/cued_rnn_$crit \
#    data/$mic/${decode_set}_hires ${decode_dir} \
#    ${decode_dir}.rnnlm.$crit.cued.lat.${ngram_order}gram
#done
