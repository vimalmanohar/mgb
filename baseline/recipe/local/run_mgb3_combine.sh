#! /bin/bash

. path.sh 
. cmd.sh

for tau in 10 15 20 5; do
  steps/train_map.sh --cmd "$train_cmd" --tau $tau \
    data/mgb3_adapt_combine data/lang \
    exp/mer80/tri5_sat_basis_ali_mgb3_adapt_combine exp/mer80_mgb3_combine/tri5_adapt_map_tau${tau}

  utils/mkgraph.sh data/lang_1200h_test exp/mer80_mgb3_combine/tri5_adapt_map_tau${tau}{,/graph_1200h}

  steps/decode_basis_fmllr.sh --cmd "$decode_cmd" --nj 40 \
    --config conf/decode.config \
    exp/mer80_mgb3_combine/tri5_adapt_map_tau${tau}/graph_1200h \
    data/mgb3_dev_Mohamed \
    exp/mer80_mgb3_combine/tri5_adapt_map_tau${tau}/decode_1200h_mgb3_dev_Mohamed

  steps/lmrescore_const_arpa.sh --cmd "$decode_cmd" \
    data/lang_1200h_test data/lang_test_fg data/mgb3_dev_Mohamed \
    exp/mer80_mgb3_combine/tri5_adapt_map_tau${tau}/decode_1200h_mgb3_dev_Mohamed{,_fg}
done

