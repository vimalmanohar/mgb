#! /bin/bash

. path.sh 
. cmd.sh

set -e -o pipefail

ROOT_DIR=/export/a15/vmanoha1/MGB

false && {
for x in Alaa Ali Mohamed Omar; do 
  local/mgb3_data_prep.sh $ROOT_DIR/adapt.20170322/wav \
    $ROOT_DIR/adapt.20170322/${x} data/mgb3_adapt_${x}
done

x=Mohamed
steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 40 \
  data/mgb3_adapt_${x} exp/mer80/make_mfcc/mgb3_adapt_${x} mfcc
steps/compute_cmvn_stats.sh \
  data/mgb3_adapt_${x} exp/mer80/make_mfcc/mgb3_adapt_${x} mfcc

for x in Alaa Ali Mohamed Omar; do
  if [ $x != "Mohamed" ]; then
    cp data/mgb3_adapt_Mohamed/{feats.scp,cmvn.scp} data/mgb3_adapt_${x}
  fi
  utils/fix_data_dir.sh data/mgb3_adapt_${x}
  utils/copy_data_dir.sh --utt-prefix ${x}- --spk-prefix ${x}- \
    data/mgb3_adapt_${x} data/mgb3_adapt_${x}.prefix

  paste -d ' ' data/mgb3_adapt_${x}.prefix/utt2spk data/mgb3_adapt_${x}/utt2spk  | \
    cut -d ' ' -f 1,3 > data/mgb3_adapt_${x}.prefix/utt2uniq
done

utils/combine_data.sh \
  data/mgb3_adapt_combine data/mgb3_adapt_{Alaa,Ali,Mohamed,Omar}.prefix
rm -r data/mgb3_adapt_{Alaa,Ali,Mohamed,Omar}.prefix

steps/align_basis_fmllr.sh --nj 40 --cmd "$train_cmd" \
  data/mgb3_adapt_combine data/lang exp/mer80/tri5_sat_basis exp/mer80/tri5_sat_basis_ali_mgb3_adapt_combine

steps/train_map.sh --cmd "$train_cmd" \
  data/mgb3_adapt_combine data/lang \
  exp/mer80/tri5_sat_basis_ali_mgb3_adapt_combine exp/mer80_mgb3_combine/tri5_adapt_map

for x in Alaa Ali Mohamed Omar; do 
  local/mgb3_data_prep.sh $ROOT_DIR/dev.20170322/wav \
    $ROOT_DIR/dev.20170322/${x} data/mgb3_dev_${x}
done

x=Mohamed
steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 40 \
  data/mgb3_dev_${x} exp/mer80/make_mfcc/mgb3_dev_${x} mfcc
steps/compute_cmvn_stats.sh \
  data/mgb3_dev_${x} exp/mer80/make_mfcc/mgb3_dev_${x} mfcc

for x in Alaa Ali Mohamed Omar; do
  if [ $x != "Mohamed" ]; then
    cp data/mgb3_dev_Mohamed/{feats.scp,cmvn.scp} data/mgb3_dev_${x}
  fi
  utils/fix_data_dir.sh data/mgb3_dev_${x}
  utils/copy_data_dir.sh --utt-prefix ${x}- --spk-prefix ${x}- \
    data/mgb3_dev_${x} data/mgb3_dev_${x}.prefix

  paste -d ' ' data/mgb3_dev_${x}.prefix/utt2spk data/mgb3_dev_${x}/utt2spk  | \
    cut -d ' ' -f 1,3 > data/mgb3_dev_${x}.prefix/utt2uniq
done

cat <<EOF > data/mgb3_dev_Mohamed/mrwer_refs
$ROOT_DIR/dev.20170322/Alaa/text_noverlap.bw
$ROOT_DIR/dev.20170322/Ali/text_noverlap.bw
$ROOT_DIR/dev.20170322/Mohamed/text_noverlap.bw
$ROOT_DIR/dev.20170322/Omar/text_noverlap.bw
EOF

for tau in 10 15 20 5; do
  steps/train_map.sh --cmd "$train_cmd" --tau $tau \
    data/mgb3_adapt_combine data/lang \
    exp/mer80/tri5_sat_basis_ali_mgb3_adapt_combine exp/mer80_mgb3_combine/tri5_adapt_map_tau${tau}

  utils/mkgraph.sh data/lang_1200h_test exp/mer80_mgb3_combine/tri5_adapt_map_tau${tau}{,/graph_1200h}

  steps/decode_basis_fmllr.sh --cmd "$decode_cmd" --nj 40 \
    --config conf/decode.config \
    exp/mer80_mgb3_combine/tri5_adapt_map_tau${tau}/graph_1200h \
    data/mgb3_dev_Mohamed \
    exp/mer80_mgb3_combine/tri5_adapt_map_tau${tau}/decode_1200h_mgb3_dev_Mohamed

  steps/lmrescore_const_arpa.sh --cmd "$decode_cmd" \
    data/lang_1200h_test data/lang_test_fg data/mgb3_dev_Mohamed \
    exp/mer80_mgb3_combine/tri5_adapt_map_tau${tau}/decode_1200h_mgb3_dev_Mohamed{,_fg}
done
}

dir=`pwd`/exp/mer80_mgb3_fused/mgb3_adapt_graphs
mkdir -p $dir

false && {
utils/split_data.sh --per-utt data/mgb3_adapt_Mohamed 80

$train_cmd JOB=1:80 $dir/log/fuse_text.JOB.log \
  steps/combine_texts_to_graph.py data/lang \
  data/mgb3_adapt_Mohamed/split80utt/JOB/text \
  data/mgb3_adapt_{Alaa,Ali,Omar}/text - \| \
  fstcopy ark,t:- ark,scp:$dir/G.tmp.JOB.ark,$dir/G.tmp.JOB.scp

for n in `seq 80`; do
  awk '{print $1" fstdeterminizestar --use-log "$2" |"}' $dir/G.tmp.$n.scp > \
    $dir/G.tmp.det.$n.scp
done

$train_cmd JOB=1:80 $dir/log/determinize.JOB.log \
  fstcopy scp:$dir/G.tmp.det.JOB.scp \
  ark,scp:$dir/G.JOB.ark,$dir/G.JOB.scp

for n in `seq 80`; do
  cat $dir/G.$n.scp
done | sort -k1,1 > $dir/G.scp

steps/align_basis_fmllr.sh --nj 40 --cmd "$train_cmd" \
  --graphs-scp exp/mer80_mgb3_fused/mgb3_adapt_graphs/G.scp \
  data/mgb3_adapt_Mohamed data/lang exp/mer80/tri5_sat_basis exp/mer80/tri5_sat_basis_ali_sausage_mgb3_adapt_Mohamed

steps/train_map.sh --cmd "$train_cmd" \
  data/mgb3_adapt_Mohamed data/lang \
  exp/mer80/tri5_sat_basis_ali_sausage_mgb3_adapt_Mohamed exp/mer80_mgb3_fused/tri5_adapt_map
cp exp/mer80/tri5_sat_basis/fmllr.basis exp/mer80_mgb3_fused/tri5_adapt_map

utils/mkgraph.sh data/lang_1200h_test exp/mer80_mgb3_fused/tri5_adapt_map{,/graph_1200h}

steps/decode_basis_fmllr.sh --cmd "$decode_cmd" --nj 40 \
  --config conf/decode.config \
  exp/mer80_mgb3_fused/tri5_adapt_map/graph_1200h \
  data/mgb3_dev_Mohamed \
  exp/mer80_mgb3_fused/tri5_adapt_map/decode_1200h_mgb3_dev_Mohamed

steps/align_basis_fmllr_lats.sh --nj 40 --cmd "$train_cmd" \
  --graphs-scp exp/mer80_mgb3_fused/mgb3_adapt_graphs/G.scp \
  data/mgb3_adapt_Mohamed data/lang exp/mer80/tri5_sat_basis \
  exp/mer80/tri5_sat_basis_lats_sausage_mgb3_adapt_Mohamed
}

for tau in 10 15 20; do
  steps/train_map_lats.sh --cmd "$train_cmd" --tau $tau \
    data/mgb3_adapt_Mohamed data/lang \
    exp/mer80/tri5_sat_basis_lats_sausage_mgb3_adapt_Mohamed \
    exp/mer80_mgb3_fused/tri5_adapt_map_tau$tau &
done
wait

for tau in 10 15 20; do
  utils/mkgraph.sh data/lang_1200h_test exp/mer80_mgb3_fused/tri5_adapt_map_tau${tau}{,/graph_1200h}

  steps/decode_basis_fmllr.sh --cmd "$decode_cmd" --nj 40 \
    --config conf/decode.config \
    exp/mer80_mgb3_fused/tri5_adapt_map_tau${tau}/graph_1200h \
    data/mgb3_dev_Mohamed \
    exp/mer80_mgb3_fused/tri5_adapt_map_tau${tau}/decode_1200h_mgb3_dev_Mohamed
done

false && {
steps/align_basis_fmllr.sh --nj 40 --cmd "$train_cmd" \
  --graphs-scp exp/mer80_mgb3_fused/mgb3_adapt_graphs/G.scp \
  data/mgb3_adapt_Mohamed data/lang exp/mer80_mgb3_combine/tri5_adapt_map \
  exp/mer80_mgb3_combine/tri5_adapt_map_ali_sausage_mgb3_adapt_Mohamed

steps/make_denlats.sh --cmd "$decode_cmd" --nj 40 \
  --text data/mgb3_adapt_combine/text \
  --transform-dir exp/mer80_mgb3_combine/tri5_adapt_map_ali_sausage_mgb3_adapt_Mohamed \
  data/mgb3_adapt_Mohamed data/lang \
  exp/mer80_mgb3_combine/tri5_adapt_map \
  exp/mer80_mgb3_combine/tri5_adapt_map_denlats_mgb3_adapt_Mohamed

steps/train_mmi.sh --cmd "$train_cmd" --boost 0.1 \
  data/mgb3_adapt_Mohamed data/lang \
  exp/mer80_mgb3_combine/tri5_adapt_map_ali_sausage_mgb3_adapt_Mohamed \
  exp/mer80_mgb3_combine/tri5_adapt_map_denlats_mgb3_adapt_Mohamed \
  exp/mer80_mgb3_combine/tri5_adapt_map_fused_mmi
  
utils/mkgraph.sh data/lang_1200h_test exp/mer80_mgb3_combine/tri5_adapt_map_fused_mmi{,/graph_1200h}

steps/decode_basis_fmllr.sh --cmd "$decode_cmd" --nj 40 \
  --config conf/decode.config \
  exp/mer80_mgb3_combine/tri5_adapt_map_fused_mmi/graph_1200h \
  data/mgb3_dev_Mohamed \
  exp/mer80_mgb3_combine/tri5_adapt_map_fused_mmi/decode_1200h_mgb3_dev_Mohamed

steps/align_basis_fmllr.sh --nj 40 --cmd "$train_cmd" \
  data/mgb3_adapt_combine data/lang exp/mer80_mgb3_combine/tri5_adapt_map \
  exp/mer80_mgb3_combine/tri5_adapt_map_ali_mgb3_adapt_combine

steps/make_denlats.sh --cmd "$decode_cmd" --nj 40 \
  --transform-dir exp/mer80_mgb3_combine/tri5_adapt_map_ali_mgb3_adapt_combine \
  data/mgb3_adapt_combine data/lang \
  exp/mer80_mgb3_combine/tri5_adapt_map \
  exp/mer80_mgb3_combine/tri5_adapt_map_denlats_mgb3_adapt_combine

steps/train_mmi.sh --cmd "$train_cmd" --boost 0.1 \
  data/mgb3_adapt_combine data/lang \
  exp/mer80_mgb3_combine/tri5_adapt_map_ali_mgb3_adapt_combine \
  exp/mer80_mgb3_combine/tri5_adapt_map_denlats_mgb3_adapt_combine \
  exp/mer80_mgb3_combine/tri5_adapt_map_combine_mmi_b0.1
}

false && {
utils/mkgraph.sh data/lang_1200h_test exp/mer80_mgb3_combine/tri5_adapt_map_combine_mmi_b0.1{,/graph_1200h}

steps/decode_basis_fmllr.sh --cmd "$decode_cmd" --nj 40 \
  --config conf/decode.config \
  exp/mer80_mgb3_combine/tri5_adapt_map_combine_mmi_b0.1/graph_1200h \
  data/mgb3_dev_Mohamed \
  exp/mer80_mgb3_combine/tri5_adapt_map_combine_mmi_b0.1/decode_1200h_mgb3_dev_Mohamed
}
