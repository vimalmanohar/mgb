#! /bin/bash

# Copyright 2017
# Vimal Manohar

set -e -o pipefail -u

extra_left_context=50
extra_right_context=0
frames_per_chunk=150

datasets=mgb3_dev_Mohamed_spkutt

graph_dir=    # If not provided, $chain_dir/graph_1200h will be used
extractor=exp/nnet3_mer80/extractor   # use previously trained extractor
dir=exp/chain_segmented_1e_adapt_mgb3/tdnn_lstm_1c_disc_1g_sp_bi

remove_oov=false
lang_test=data/lang_1200h_test   # For creating lattices

lang_rescore=data/lang_test_fg   # For rescoring
rescore_suffix=fg 

decode_nj=40

. cmd.sh
. ./path.sh
. ./utils/parse_options.sh

if [ -z "$graph_dir" ]; then
  graph_dir=$dir/graph_1200h
fi

if $remove_oov; then
  graph_dir=${graph_dir}_nooov
fi

if [ ! -f $graph_dir/HCLG.fst ]; then
  # Note: it might appear that this data/lang_chain directory is mismatched, and it is as
  # far as the 'topo' is concerned, but this script doesn't read the 'topo' from
  # the lang directory.
  if $remove_oov; then
    utils/mkgraph.sh --left-biphone --self-loop-scale 1.0 --remove-oov $lang_test $dir $graph_dir 
  else
    utils/mkgraph.sh --left-biphone --self-loop-scale 1.0 $lang_test $dir $graph_dir 
  fi
fi

ivectors_dir=`dirname $extractor`
for dev in $datasets; do
  if [ ! -f $ivectors_dir/ivectors_$dev/.done ]; then
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj 40 \
      data/${dev}_hires $extractor $ivectors_dir/ivectors_$dev
    touch $ivectors_dir/ivectors_$dev/.done
  fi
  graph_suffix=${graph_dir#*graph}
  decode_dir=$dir/decode${graph_suffix}_$dev

  if [ ! -f $decode_dir/.done ]; then
    steps/nnet3/decode.sh --num-threads 4 --nj $decode_nj --cmd "$decode_cmd" \
      --acwt 1.0 --post-decode-acwt 10.0 \
      --extra-left-context $extra_left_context  \
      --extra-right-context $extra_right_context  \
      --frames-per-chunk "$frames_per_chunk" \
      --online-ivector-dir $ivectors_dir/ivectors_$dev \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" \
      $dir/graph_1200h data/${dev}_hires $decode_dir || exit 1;
    touch $decode_dir/.done
  fi

  if [ ! -f ${decode_dir}_${rescore_suffix}/.done ]; then
    steps/lmrescore_const_arpa.sh --cmd "$decode_cmd" \
      --scoring-opts "--min-lmwt 5 --max-lmwt 15" \
      $lang_test $lang_rescore \
      data/${dev}_hires ${decode_dir}{,_${rescore_suffix}} || exit 1
    touch ${decode_dir}_${rescore_suffix}/.done
  fi
done
