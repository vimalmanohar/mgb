#! /bin/bash

dir=exp/large_text
data=data/mgb3_adapt_Mohamed
stage=2
num_docs=10
nj=10

. path.sh
. cmd.sh 

set -e -o pipefail -u

. utils/parse_options.sh

mkdir -p $dir

if [ $stage -le 0 ]; then
  [ -z "$LM_DIR" ] && echo "LM_DIR must be defined." && exit 1
  lm_text=$LM_DIR/mgb.arabic.normalized.bukwalter

  awk '{i++; print "foo-"i" "$0}' $lm_text > $dir/docs.txt
fi

  num_jobs=1000
if [ $stage -le 1 ]; then
  mkdir -p $dir/split${num_jobs}
  split_scps=
  for n in `seq $num_jobs`; do
    split_scps="$split_scps $dir/split$num_jobs/docs.$n.txt"
  done
  utils/split_scp.pl $dir/docs.txt $split_scps

  $train_cmd JOB=1:1000 $dir/log/compute_source_tf_idf.JOB.log \
    steps/cleanup/internal/compute_tf_idf.py \
      --tf-weighting-scheme=raw --idf-weighting-scheme=log \
      --output-idf-stats=$dir/split$num_jobs/idf_stats.JOB.txt \
      --output-tf-stats=$dir/split$num_jobs/tf_stats.JOB.txt \
      $dir/split$num_jobs/docs.JOB.txt $dir/src_tf_idf.JOB.txt || exit 1
fi

if [ $stage -le 2 ]; then
  for n in `seq $num_jobs`; do
    cat $dir/split${num_jobs}/tf_stats.$nj.txt
  done > $dir/tf_stats.txt
fi 

if [ $stage -le 3 ]; then
  for n in `seq $num_jobs`; do
    cat $dir/split$num_jobs/idf_stats.$n.txt
  done > $dir/split$num_jobs/idf_stats.combined.txt
  
  cat $dir/split$num_jobs/idf_stats.combined.txt | \
    python -c 'import sys, collections
counts = collections.defaultdict(int)
for line in sys.stdin.readlines():
  parts = line.strip().split()
  if len(parts) == 2:
    counts[parts[0]] += int(parts[1])

for w, count in counts.iteritems():
  print ("{0} {1}".format(w, count))' > $dir/idf_stats.txt
fi

if [ $stage -le 4 ]; then
  $train_cmd $dir/log/compute_source_tf_idf.log \
    steps/cleanup/internal/compute_tf_idf_from_stats.py --ngram-order=1 \
    --tf-weighting-scheme=raw --idf-weighting-scheme=log \
    $dir/tf_stats.txt $dir/idf_stats.txt \
    $dir/src_tf_idf.txt
fi

workdir=$dir/`basename $data`
if [ $stage -le 5 ]; then
  mkdir -p $workdir
  utils/data/convert_data_dir_to_whole.sh $data $workdir

  utils/split_data.sh $workdir $nj

  $train_cmd JOB=1:$nj $workdir/log/compute_query_tf_idf.JOB.log \
    steps/cleanup/internal/compute_tf_idf.py \
      --tf-weighting-scheme="normalized" \
      --idf-weighting-scheme="log" --ngram-order=1 \
      --input-idf-stats=$dir/idf_stats.txt \
      --accumulate-over-docs=false \
      $workdir/split${nj}/JOB/text $workdir/split${nj}/JOB/query_tf_idf.JOB.ark.txt
fi


if [ $stage -le 6 ]; then
  echo "foo" $(cut -d ' ' -f 1 $dir/docs.txt | tr '\n' ' ') > $dir/text2doc
  echo "foo $dir/src_tf_idf.txt" > $dir/source2tf_idf.scp
  awk '{print $1" foo"}' $workdir/text > $workdir/new2orig_utt

  $train_cmd --mem 8G JOB=1:$nj $dir/log/retrieve_similar_docs.JOB.log \
    steps/cleanup/internal/retrieve_similar_docs.py \
    --query-tfidf=$workdir/split${nj}/JOB/query_tf_idf.JOB.ark.txt \
    --source-text-id2tfidf=$dir/source2tf_idf.scp \
    --source-text-id2doc-ids=$dir/text2doc \
    --query-id2source-text-id=$workdir/new2orig_utt \
    --get-top-n-docs=$num_docs \
    --relevant-docs=$workdir/split${nj}/JOB/relevant_docs.JOB.txt
fi

if [ $stage -le 7 ]; then
  $train_cmd JOB=1:$nj $dir/log/stitch_docs.JOB.log \
    steps/cleanup/internal/stitch_documents.py \
    --query2docs=$workdir/split${nj}/JOB/relevant_docs.JOB.txt \
    --input-documents=$dir/docs.txt \
    --output-documents=$workdir/split${nj}/JOB/retrieved_docs.JOB.txt
fi

if [ $stage -le 8 ]; then
  for n in `seq $nj`; do
    cat $workdir/split${nj}/$n/retrieved_docs.$n.txt
  done > $workdir/retrieved_docs.txt
fi
