#! /bin/bash

. ./cmd.sh
. ./path.sh

set -e -o pipefail -u

segment_stage=-10
affix=_1c
decode_nj=30
cleanup_stage=-10

. utils/parse_options.sh

###############################################################################
# Segment long recordings using TF-IDF retrieval of reference text 
# for uniformly segmented audio chunks based on Smith-Waterman alignment.
# Use a model trained on train_mer80 (250 hours)
###############################################################################

# This is similar to _c, but uses basis FMLLR to do the decoding during
# cleanup.

###
# STAGE 0
###

if [ ! -s data/train_orig/utt2spk ]; then
  WAV_DIR=/export/a15/vmanoha1/MGB/audio/ 
  textDir=/export/a15/vmanoha1/MGB/originaltxt_bw/train
  local/mgb_prep_original_data.sh $WAV_DIR $textDir 
fi

if [ ! -s data/train_orig/feats.scp ]; then
  steps/make_mfcc.sh --nj 40 --cmd "$train_cmd" --write-utt2num-frames true \
    data/train_orig exp/make_mfcc/train_orig mfcc
  steps/compute_cmvn_stats.sh \
    data/train_orig exp/make_mfcc/train_orig mfcc
  utils/fix_data_dir.sh data/train_orig
fi

gmm_dir=exp/mer80/tri4
if [ ! -f $gmm_dir/final.mdl ]; then
  echo "$0: Could not find $gmm_dir/final.mdl" && exit 1
fi

false && {
steps/cleanup/segment_long_utterances.sh \
  --cmd "$train_cmd" --nj 80 \
  --stage $segment_stage \
  --max-bad-proportion 0.5 --max-wer 50 --align-full-hyp false \
  $gmm_dir data/lang data/train_orig data/train_orig/text \
  data/train_reseg${affix} \
  exp/segment_long_utts${affix}_train_orig

steps/compute_cmvn_stats.sh \
  data/train_reseg${affix} exp/make_mfcc/train_reseg${affix} mfcc
utils/fix_data_dir.sh data/train_reseg${affix}

steps/align_basis_fmllr.sh --nj 80 --cmd "$train_cmd" \
  data/train_reseg${affix} data/lang \
  exp/mer80/tri4_sat_basis exp/mer80/tri4_sat_basis_ali_train_reseg${affix} || exit 1

steps/train_sat_basis.sh  --cmd "$train_cmd" 10000 150000 \
  data/train_reseg${affix} data/lang \
  exp/mer80/tri4_sat_basis_ali_train_reseg${affix} \
  exp/segmented${affix}/tri5_sat_basis || exit 1;

utils/mkgraph.sh data/lang_1200h_test exp/segmented${affix}/tri5_sat_basis{,/graph_1200h}

for dset in dev_non_overlap dev_overlap; do
  steps/decode_fmllr.sh --nj $decode_nj --cmd "$decode_cmd" --num-threads 4 \
    exp/segmented${affix}/tri5_sat_basis/graph_1200h data/$dset exp/segmented${affix}/tri5_sat_basis/decode_1200h_$dset
  steps/lmrescore_const_arpa.sh --cmd "$decode_cmd" data/lang_1200h_test data/lang_test_fg \
    data/$dset exp/segmented${affix}/tri5_sat_basis/decode_1200h_${dset}{,_fg}
done
}

###
# STAGE 3
###

srcdir=exp/segmented${affix}/tri5_sat_basis
cleanup_affix=cleaned
cleaned_data=data/train_reseg${affix}_${cleanup_affix}
cleaned_dir=${srcdir}_${cleanup_affix}_basis

false && {
steps/cleanup/clean_and_segment_data.sh --stage $cleanup_stage --nj 80 \
  --cmd "$train_cmd" \
  data/train_reseg${affix} data/lang $srcdir \
  ${srcdir}_${cleanup_affix}_work $cleaned_data

steps/align_fmllr.sh --nj 40 --cmd "$train_cmd" \
  $cleaned_data data/lang $srcdir ${srcdir}_ali_${cleanup_affix}

steps/train_sat.sh --cmd "$train_cmd" \
  10000 150000 $cleaned_data data/lang ${srcdir}_ali_${cleanup_affix} \
  ${cleaned_dir}

utils/mkgraph.sh data/lang_1200h_test $cleaned_dir ${cleaned_dir}/graph_1200h

for dset in dev_overlap dev_non_overlap; do
  steps/decode_fmllr.sh --nj $decode_nj --cmd "$decode_cmd"  --num-threads 4 \
    ${cleaned_dir}/graph_1200h data/${dset} ${cleaned_dir}/decode_1200h_${dset}
  steps/lmrescore_const_arpa.sh --cmd "$decode_cmd" data/lang_1200h_test data/lang_test_fg \
     data/${dset} ${cleaned_dir}/decode_1200h_${dset} \
     ${cleaned_dir}/decode_1200h_${dset}_fg
done
}

srcdir=exp/segmented${affix}/tri5_sat_basis
cleaned_dir=${srcdir}_${cleanup_affix}_basis

steps/align_basis_fmllr.sh --nj 40 --cmd "$train_cmd" \
  $cleaned_data data/lang $srcdir ${srcdir}_basis_ali_${cleanup_affix}

steps/train_sat_basis.sh --cmd "$train_cmd" \
  10000 150000 $cleaned_data data/lang ${srcdir}_basis_ali_${cleanup_affix} \
  ${cleaned_dir}

utils/mkgraph.sh data/lang_1200h_test $cleaned_dir ${cleaned_dir}/graph_1200h

for dset in dev_overlap dev_non_overlap; do
  steps/decode_basis_fmllr.sh --nj $decode_nj --cmd "$decode_cmd"  --num-threads 4 \
    ${cleaned_dir}/graph_1200h data/${dset} ${cleaned_dir}/decode_1200h_${dset}
  steps/lmrescore_const_arpa.sh --cmd "$decode_cmd" data/lang_1200h_test data/lang_test_fg \
     data/${dset} ${cleaned_dir}/decode_1200h_${dset} \
     ${cleaned_dir}/decode_1200h_${dset}_fg
done

exit 0

# Baseline | %WER 17.9 | 507 17783 | 85.1 10.5 4.4 3.0 17.9 90.9 | -0.055 | exp/tri3_cleaned/decode_dev_rescore/score_15_0.0/ctm.filt.filt.sys
# STAGE 0 | %WER 19.3 | 507 17783 | 83.9 10.8 5.2 3.2 19.3 92.3 | -0.178 | exp/tri4_1e/decode_dev_rescore/score_14_0.0/ctm.filt.filt.sys
# STAGE 1 | %WER 18.8 | 507 17783 | 84.4 10.7 4.9 3.2 18.8 91.7 | -0.199 | exp/tri5_2e/decode_nosp_dev_rescore/score_13_0.0/ctm.filt.filt.sys
# STAGE 2 | %WER 18.4 | 507 17783 | 84.7 10.4 4.8 3.2 18.4 91.7 | -0.192 | exp/tri5_2e/decode_dev_rescore/score_14_0.5/ctm.filt.filt.sys
# STAGE 3 | %WER 18.8 | 507 17783 | 84.4 10.7 4.9 3.2 18.8 91.3 | -0.162 | exp/tri5_2e_cleaned/decode_nosp_dev_rescore/score_14_0.0/ctm.filt.filt.sys

# Baseline | %WER 16.6 | 1155 27500 | 85.8 10.9 3.4 2.4 16.6 86.4 | -0.058 | exp/tri3_cleaned/decode_test_rescore/score_15_0.0/ctm.filt.filt.sys
# STAGE 0 | %WER 18.0 | 1155 27500 | 84.4 11.7 3.9 2.4 18.0 87.5 | -0.038 | exp/tri4_1e/decode_test_rescore/score_13_0.0/ctm.filt.filt.sys
# STAGE 1 | %WER 17.7 | 1155 27500 | 84.7 11.4 3.9 2.3 17.7 87.0 | -0.044 | exp/tri5_2e/decode_nosp_test_rescore/score_13_0.0/ctm.filt.filt.sys
# STAGE 2 | %WER 16.8 | 1155 27500 | 85.7 11.0 3.3 2.5 16.8 86.6 | -0.066 | exp/tri5_2e/decode_test_rescore/score_14_0.0/ctm.filt.filt.sys
# STAGE 3 | %WER 17.4 | 1155 27500 | 85.0 11.4 3.7 2.4 17.4 87.2 | -0.086 | exp/tri5_2e_cleaned/decode_nosp_test_rescore/score_12_0.0/ctm.filt.filt.sys

# Baseline | %WER 19.0 | 507 17783 | 83.9 11.4 4.7 2.9 19.0 92.1 | -0.054 | exp/tri3_cleaned/decode_dev/score_13_0.5/ctm.filt.filt.sys
# STAGE 0 | %WER 20.5 | 507 17783 | 82.8 11.9 5.3 3.3 20.5 94.1 | -0.098 | exp/tri4_1e/decode_dev/score_14_0.0/ctm.filt.filt.sys
# STAGE 1 | %WER 19.8 | 507 17783 | 83.3 11.5 5.2 3.2 19.8 94.7 | -0.133 | exp/tri5_2e/decode_nosp_dev/score_14_0.0/ctm.filt.filt.sys
# STAGE 2 | %WER 19.5 | 507 17783 | 83.9 11.4 4.7 3.5 19.5 94.1 | -0.120 | exp/tri5_2e/decode_dev/score_16_0.0/ctm.filt.filt.sys
# STAGE 3 | %WER 20.0 | 507 17783 | 83.5 11.7 4.8 3.5 20.0 93.3 | -0.111 | exp/tri5_2e_cleaned/decode_nosp_dev/score_13_0.0/ctm.filt.filt.sys

# Baseline | %WER 17.6 | 1155 27500 | 84.8 11.7 3.5 2.4 17.6 87.6 | 0.001 | exp/tri3_cleaned/decode_test/score_15_0.0/ctm.filt.filt.sys
# STAGE 0 | %WER 19.1 | 1155 27500 | 83.4 12.5 4.1 2.5 19.1 88.6 | 0.022 | exp/tri4_1e/decode_test/score_13_0.0/ctm.filt.filt.sys
# STAGE 1 | %WER 18.7 | 1155 27500 | 83.7 12.2 4.1 2.4 18.7 88.1 | 0.007 | exp/tri5_2e/decode_nosp_test/score_13_0.0/ctm.filt.filt.sys
# STAGE 2 | %WER 17.9 | 1155 27500 | 84.8 11.9 3.3 2.7 17.9 87.5 | -0.015 | exp/tri5_2e/decode_test/score_13_0.0/ctm.filt.filt.sys
# STAGE 3 | %WER 18.4 | 1155 27500 | 83.9 12.1 4.0 2.3 18.4 88.1 | -0.015 | exp/tri5_2e_cleaned/decode_nosp_test/score_13_0.0/ctm.filt.filt.sys

